﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class TransactionTest
    {
        [Test]
        public void Transaction()
        {
            Transaction t = new Transaction(5, new DateTime(2017, 12, 31));
            Assert.AreEqual(true, t is Transaction);
        }
    }
}
