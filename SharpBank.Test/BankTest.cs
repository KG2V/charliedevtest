﻿using NUnit.Framework;
using System;

//Major testability issue - the transactions are all added on the date this program is run, making it near impossible
//we need to refactor that


namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private const decimal DECIMAL_DELTA = .0001M;
        private AccountFactory factory = new AccountFactory();
    

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");

            john.OpenAccount(factory.CreateAccount(AccountFactory.AccountType.Checking));
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount365Days()
        {
            DateTime periodStartDate = new DateTime(2017, 01, 01);
            DateTime periodEndDate = new DateTime(2017, 012, 31);

            Bank bank = new Bank();
            AccountFactory factory = new AccountFactory();
            Account checkingAccount = factory.CreateAccount(AccountFactory.AccountType.Checking);
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0M, new DateTime(2017,01,01));

            AssertDiff(.1000M, bank.TotalInterestPaid(periodStartDate, periodEndDate), DECIMAL_DELTA);
        }

        [Test]
        public void CheckingAccount1Day()
        {
            DateTime periodStartDate = new DateTime(2017, 10, 01);
            DateTime periodEndDate = new DateTime(2017, 10, 01);

            Bank bank = new Bank();
            AccountFactory factory = new AccountFactory();
            Account checkingAccount = factory.CreateAccount(AccountFactory.AccountType.Checking);
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.00M, new DateTime(2017, 10, 01));
            AssertDiff(.0003M, bank.TotalInterestPaid(periodStartDate, periodEndDate),DECIMAL_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            DateTime periodStartDate = new DateTime(2017, 01, 01);
            DateTime periodEndDate = new DateTime(2017, 12, 31);


            Bank bank = new Bank();
            Account  savingsAccount = factory.CreateAccount(AccountFactory.AccountType.Savings);
            bank.AddCustomer(new Customer("Bill").OpenAccount(savingsAccount));

            savingsAccount.Deposit(1500.0M, new DateTime(2017, 12, 31));

            AssertDiff(3.0029M, bank.TotalInterestPaid(periodStartDate, periodEndDate), DECIMAL_DELTA);
        }

        [Test]
        public void MaxiSavingsAccount()
        {
            DateTime periodStartDate = new DateTime(2017, 01, 01);
            DateTime periodEndDate = new DateTime(2017, 12, 31);


            Bank bank = new Bank();
            AccountFactory factory = new AccountFactory();
            Account checkingAccount = factory.CreateAccount(AccountFactory.AccountType.MaxiSavings);
            bank.AddCustomer(new Customer("Bill").OpenAccount(checkingAccount));

            checkingAccount.Deposit(3000.0M,new DateTime(2017, 12, 31));

            AssertDiff(170.0M, bank.TotalInterestPaid(periodStartDate, periodEndDate), DECIMAL_DELTA);
        }


        void AssertDiff(decimal a, decimal b, decimal diff)
        {
            Assert.IsTrue(Math.Abs(a - b) < diff);
        }
    }
}
