﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {
        private AccountFactory factory = new AccountFactory();

        [Test]
        public void TestCustomerStatementGeneration()
        {
           
            Account checkingAccount = factory.CreateAccount(AccountFactory.AccountType.Checking);
            Account savingsAccount = factory.CreateAccount(AccountFactory.AccountType.Savings);
            
            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0M, new DateTime(2017, 10, 1));
            savingsAccount.Deposit(4000.0M, new DateTime(2017, 10, 5));
            savingsAccount.Withdraw(200.0M, new DateTime(2017, 10, 10));

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00", henry.GetStatement());
        }

        [Test]
        public void TestOneAccount()
        {
           
            Customer oscar = new Customer("Oscar").OpenAccount(factory.CreateAccount(AccountFactory.AccountType.Savings));
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
           
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(factory.CreateAccount(AccountFactory.AccountType.Savings));
            oscar.OpenAccount(factory.CreateAccount(AccountFactory.AccountType.Checking));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(factory.CreateAccount(AccountFactory.AccountType.Savings));
            oscar.OpenAccount(factory.CreateAccount(AccountFactory.AccountType.Checking));
            oscar.OpenAccount(factory.CreateAccount(AccountFactory.AccountType.MaxiSavings));

            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }
    }
}
