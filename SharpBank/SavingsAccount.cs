﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class SavingsAccount : Account
    {

        
        internal SavingsAccount()
        {
        }

        public override decimal InterestEarned(DateTime periodStartDate, DateTime periodEndDate)
        {

            decimal totalInterest = 0;

            //Question for Business Analysit
            //Person deposits money on day N, and also withdraws the money on day N - How do we 
            //want to deal with interest?
            //as I have no current answer, I will credit them
            //For preiod End, we have to go end of day so...

            DateTime endDate = periodEndDate.Date.AddDays(1);
            for (DateTime currentDate = periodStartDate.Date; currentDate.Date < endDate.Date; currentDate = currentDate.AddDays(1))
            {
                decimal todaysBalance = BalanceOnDate(currentDate);
                decimal todaysInterest = 0M;
                if (todaysBalance <= 1000)
                {
                    todaysInterest = (BalanceOnDate(currentDate) * .001M) / 365;
                }
                else
                {
                    todaysInterest = (BalanceOnDate(currentDate) * .002M) / 365;
                }
                totalInterest += todaysInterest;
                this.Deposit(todaysInterest, DateTime.Now);

            }

            return totalInterest;





            decimal amount = SumTransactions();

            if (amount <= 1000)
                return amount * 0.001M;
            else
                return 1 + (amount - 1000) * 0.002M;

        }

        public override string PrettyAccountName()
        {
            return "Savings Account\n";
        }

    }
}
