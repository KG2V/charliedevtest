﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly decimal amount;

        public DateTime transactionDate{ get; private set; }


        public Transaction(decimal amount, DateTime tranactionDate)
        {
            this.amount = amount;
            this.transactionDate = transactionDate;
        }

    }
}
