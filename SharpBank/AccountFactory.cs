﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class AccountFactory
    {
        public enum AccountType
        {
            Savings,
            Checking,
            MaxiSavings
        }

        public Account CreateAccount(AccountType accountType )
        {
            switch (accountType)
            {
                case AccountType.Savings:
                    { return new SavingsAccount(); }
                case AccountType.Checking:
                    { return new CheckingAccount(); }
                case AccountType.MaxiSavings:
                    { return new MaxiSavingsAccount(); }
                default:
                    throw new ArgumentException("Invalid Account Type");

            }
        }
    }
}
