﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class MaxiSavingsAccount : Account
    {
        internal MaxiSavingsAccount()
        {
        }

        public override decimal InterestEarned(DateTime periodStartDate, DateTime periodEndDate)
        {
            decimal amount = SumTransactions();
            //This section needs to be changed per programing spec
            //Change Maxi-Savings accounts to have an interest rate of 5 % assuming no withdrawals in the past 10 days otherwise 0.1 %
            //This becomes "Interesting" as we can have 'mixed' cases
            //Say we have 3 transactions (I'll use a Julian date number here as a example)
            //Day1 - we deposit $1000 in the account
            //we start earning 5% APR immediately - as there have been no withdrawls
            //Day5 
            //At this point we have earned ($1000 * .005)/365 * 5  or .0685
            //we make a deposit of another $1000
            //Day 15, we make a withdrawl of $500 - at this point we have earned:
            //at this point, we have earned 
            //($1000 * .005)/365 * 5 + ($2000 * .005)/365 * 4 or .0685 + .1096 or .1781
            //at day 23 you'd have
            //you have the interest from days 1-14 (.1781)
            //PLUS (2500 * .0001)/365 * 10 or .0007 + .1781 or .1788
            //Now, these numbers are off a bit, as I have not creditied the amount accured daily, but is the approximate logic


            if (amount <= 1000M)
                return amount * 0.02M;
            if (amount <= 2000)
                return 20 + (amount - 1000) * 0.05M;
            return 70 + (amount - 2000) * 0.1M;


        }

        public override string PrettyAccountName()
        {
            return "Maxi Savings Account\n";
        }
    }
}
