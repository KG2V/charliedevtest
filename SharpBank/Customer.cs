﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Customer
    {
        private String name;
        private List<Account> accounts;

        public Customer(String name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        public String GetName()
        {
            return name;
        }

        public Customer OpenAccount(Account account)
        {
            accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public decimal TotalInterestEarned(DateTime periodStartDate, DateTime periodEndDate)
        {
            decimal total = 0;
            foreach (Account a in accounts)
                total += a.InterestEarned(periodStartDate, periodEndDate);
            return total;
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public String GetStatement()
        {
            //JIRA-123 Change by Joe Bloggs 29/7/1988 start
            String statement = null; //reset statement to null here
            //JIRA-123 Change by Joe Bloggs 29/7/1988 end
            statement = "Statement for " + name + "\n";
            decimal total = 0.0M;
            foreach (Account a in accounts)
            {
                statement += "\n" + StatementForAccount(a) + "\n";
                total += a.SumTransactions();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }

        private String StatementForAccount(Account a)
        {

            String s = a.PrettyAccountName();


            //Now total up all the transactions
            decimal total = 0.0M;
            foreach (Transaction t in a.transactions)
            {
                s += "  " + (t.amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t.amount) + "\n";
                total += t.amount;
            }
            s += "Total " + ToDollars(total);
            return s;
        }

        private String ToDollars(decimal d)
        {
            return String.Format("${0:N2}", Math.Abs(d));
        }
    }
}
