﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class CheckingAccount : Account
    {
        internal CheckingAccount()
        {
        }

        public override decimal InterestEarned(DateTime periodStartDate, DateTime periodEndDate)
        {

            decimal totalInterest = 0;

            //Question for Business Analysit
            //Person deposits money on day N, and also withdraws the money on day N - How do we 
            //want to deal with interest?
            //as I have no current answer, I will credit them
            //For preiod End, we have to go end of day so...
            
            DateTime endDate = periodEndDate.Date.AddDays(1);
            for (DateTime currentDate = periodStartDate.Date; currentDate.Date < endDate.Date; currentDate = currentDate.AddDays(1))
            {
                decimal todaysInterest = (BalanceOnDate(currentDate) * .001M) / 365;
                totalInterest += todaysInterest;
                this.Deposit(todaysInterest, DateTime.Now);

            }

            return totalInterest;

        }

        public override string PrettyAccountName()
        {
            return "Checking Account\n";
        }

       

    }
}
           
