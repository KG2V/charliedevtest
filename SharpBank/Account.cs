﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{


    public abstract  class Account
    {

        public List<Transaction> transactions;

        internal Account()
        {
          //  this.accountType = accountType;
            this.transactions = new List<Transaction>();
        }

        public void Deposit(decimal amount, DateTime transactionDate)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount, transactionDate));
            }
        }

        public void Withdraw(decimal amount, DateTime transactionDate)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(-amount, transactionDate));
            }
        }

        public abstract string PrettyAccountName();

         public abstract decimal InterestEarned(DateTime periodStartDate, DateTime periodEndDate);
  

        public decimal SumTransactions()
        {
            return CheckIfTransactionsExist(true);
        }

        private decimal CheckIfTransactionsExist(bool checkAll)
        {
            decimal amount = 0.0M;
            foreach (Transaction t in transactions)
                amount += t.amount;
            return amount;
        }


        protected decimal BalanceOnDate(DateTime theDate)
        {
            decimal temp;
            temp = (from t in transactions  where t.transactionDate.Date <= theDate select t.amount).Sum();
            return temp;
        }


    }
}
